package com.cosmolapti.android.bean;

/**
 * Created by Alena on 07.12.2014.
 */
public class Result implements Comparable<Result>
{
    String date;
    String guestTeam;
    String homeTeam;
    String teamResult;

    public Result()
    {

    }

    public Result(String date, String guestTeam, String homeTeam, String teamResult)
    {
        this.date = date;
        this.guestTeam = guestTeam;
        this.homeTeam = homeTeam;
        this.teamResult = teamResult;
    }

    public String getDate()
    {
        return date;
    }

    public void setDate(String date)
    {
        this.date = date;
    }

    public String getGuestTeam()
    {
        return guestTeam;
    }

    public void setGuestTeam(String guestTeam)
    {
        this.guestTeam = guestTeam;
    }

    public String getHomeTeam()
    {
        return homeTeam;
    }

    public void setHomeTeam(String homeTeam)
    {
        this.homeTeam = homeTeam;
    }

    public String getTeamResult()
    {
        return teamResult;
    }

    public void setTeamResult(String teamResult)
    {
        this.teamResult = teamResult;
    }

    @Override
    public String toString()
    {
        return "Result{" +
                "date='" + date + '\'' +
                ", guestTeam=" + guestTeam +
                ", homeTeam=" + homeTeam +
                ", teamResult='" + teamResult + '\'' +
                '}';
    }

    @Override
    public int compareTo(Result another)
    {
        Long result = Long.valueOf(this.getDate()) - Long.valueOf(another.getDate());
        if (result == 0)
        {
            if (this.getHomeTeam().length() - another.getHomeTeam().length() == 0)
                return 0;
            else
                return (this.getHomeTeam().length() - another.getHomeTeam().length() > 0) ? -1 : 1;
        }
        else return (result > 0) ? -1 : 1;
    }
}

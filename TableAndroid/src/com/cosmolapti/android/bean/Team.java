package com.cosmolapti.android.bean;

/**
 * Created by Treshchev on 17.11.2014.
 */
public class Team implements Comparable<Team>
{
    String name;
    String place;
    String gamesPlayed;
    String points;
    String win;
    String draw;
    String lose;
    String goalsScored;
    String goalsLose;

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getPlace()
    {
        return place;
    }

    public void setPlace(String place)
    {
        this.place = place;
    }

    public String getGamesPlayed()
    {
        return gamesPlayed;
    }

    public void setGamesPlayed(String gamesPlayed)
    {
        this.gamesPlayed = gamesPlayed;
    }

    public String getPoints()
    {
        return points;
    }

    public void setPoints(String points)
    {
        this.points = points;
    }

    public String getWin()
    {
        return win;
    }

    public void setWin(String win)
    {
        this.win = win;
    }

    public String getDraw()
    {
        return draw;
    }

    public void setDraw(String draw)
    {
        this.draw = draw;
    }

    public String getLose()
    {
        return lose;
    }

    public void setLose(String lose)
    {
        this.lose = lose;
    }

    public String getGoalsScored()
    {
        return goalsScored;
    }

    public void setGoalsScored(String goalsScored)
    {
        this.goalsScored = goalsScored;
    }

    public String getGoalsLose()
    {
        return goalsLose;
    }

    public void setGoalsLose(String goalsLose)
    {
        this.goalsLose = goalsLose;
    }

    @Override
    public String toString()
    {
        return "Team{" +
                "name='" + name + '\'' +
                ", gamesPlayed='" + gamesPlayed + '\'' +
                ", points='" + points + '\'' +
                ", win='" + win + '\'' +
                ", draw='" + draw + '\'' +
                ", lose='" + lose + '\'' +
                ", goalsScored='" + goalsScored + '\'' +
                ", goalsLose='" + goalsLose + '\'' +
                '}';
    }

    @Override
    public int compareTo(Team another)
    {
        return(Integer.valueOf(place) - Integer.valueOf(another.place));
    }
}

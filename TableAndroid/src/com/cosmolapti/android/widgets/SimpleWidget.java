package com.cosmolapti.android.widgets;

import java.util.Arrays;

import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;
import android.widget.RemoteViews;
import com.cosmolapti.android.R;
import com.cosmolapti.android.bean.Team;
import com.cosmolapti.android.tools.DataStorage;
import com.cosmolapti.android.tools.Service;

public class SimpleWidget extends AppWidgetProvider {

    final static String LOG_TAG = "ALTR 777";

    @Override
    public void onEnabled(Context context) {
        super.onEnabled(context);
        Log.d(LOG_TAG, "onEnabled");
    }

    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager,
                         int[] appWidgetIds) {
        super.onUpdate(context, appWidgetManager, appWidgetIds);
        Log.d(LOG_TAG, "onUpdate " + Arrays.toString(appWidgetIds));

        SharedPreferences sp = context.getSharedPreferences(
                WidgetActivity.WIDGET_PREF, Context.MODE_PRIVATE);
        for (int id : appWidgetIds) {
            updateWidget(context, appWidgetManager, sp, id);
        }
    }
    @Override
    public void onDeleted(Context context, int[] appWidgetIds) {
        super.onDeleted(context, appWidgetIds);
        Log.d(LOG_TAG, "onDeleted " + Arrays.toString(appWidgetIds));
    }

    @Override
    public void onDisabled(Context context) {
        super.onDisabled(context);
        Log.d(LOG_TAG, "onDisabled");
    }

    static void updateWidget(Context context, AppWidgetManager appWidgetManager,
                             SharedPreferences sp, int widgetID) {
        Log.d(LOG_TAG, "updateWidget " + widgetID);

        // Читаем параметры Preferences
        String teamName = sp.getString(WidgetActivity.WIDGET_TEXT + widgetID, null);
        if (teamName == null) return;

        DataStorage dataStorage = DataStorage.getInstance();
        if (!dataStorage.isAllDataUploaded())
        {
            dataStorage.loadData(context);
        }
        Team team = dataStorage.getTeamInfo(teamName);


        // Настраиваем внешний вид виджета
        RemoteViews widgetView = new RemoteViews(context.getPackageName(),
                R.layout.simplewidget);
        widgetView.setTextViewText(R.id.teamPlace, team.getPlace());
        widgetView.setTextViewText(R.id.teamPoints, team.getPoints());
        widgetView.setTextViewText(R.id.teamTitle, Service.getRussianTitle(team.getName()));
        widgetView.setImageViewResource(R.id.teamLogo, Service.getResID(team.getName()));

        // Обновляем виджет
        appWidgetManager.updateAppWidget(widgetID, widgetView);
    }

}

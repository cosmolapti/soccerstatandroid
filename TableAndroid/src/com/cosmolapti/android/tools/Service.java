package com.cosmolapti.android.tools;

import android.os.StrictMode;
import com.cosmolapti.android.R;
import com.cosmolapti.android.bean.Result;
import com.cosmolapti.android.bean.Team;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by Treshchev on 17.11.2014.
 */
public class Service
{
    private static JSONObject JSONArrayFromServer;
    public static final String ALL_TEAMS = "http://rfpltable.appspot.com/rest/rfpl/team/";
    public static final String LAST_RESULT = "http://rfpltable.appspot.com/rest/rfpl/lastresults";
    public static final String FUTURE_GAMES = "http://rfpltable.appspot.com/rest/rfpl/futureresults";

    @Deprecated
    public static List<Team> getAllTeam()
    {
        List<Team> allTeam = new ArrayList<Team>();
        for (int i = 0; i < 18; i++)
        {
            Team testTeam1 = new Team();
            testTeam1.setPlace("" + (i + 1));
            testTeam1.setName("Test" + i);
            testTeam1.setPoints("" + (30 + i));
            testTeam1.setWin("" + (10 + i));
            testTeam1.setLose("" + (20 + i));
            testTeam1.setDraw("" + (5 + i));
            testTeam1.setGamesPlayed("" + (13 + i));
            testTeam1.setGoalsScored("" + (34 + i));
            testTeam1.setGoalsLose("" + (25 + i));

            allTeam.add(testTeam1);
        }
        Collections.sort(allTeam);
        return allTeam;

    }

    private static JSONObject getAllTeamsFromServer()
    {
        return getJSONFromServer(ALL_TEAMS);
    }

    private static JSONObject getLastResultsFromServer()
    {
        return getJSONFromServer(LAST_RESULT);
    }

    private static JSONObject getFutureGamesFromServer()
    {
        return getJSONFromServer(FUTURE_GAMES);
    }

    private static JSONObject getJSONFromServer(String stringUrl)
    {
        JSONObject jsonObject = null;
        if (android.os.Build.VERSION.SDK_INT > 9)
        {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);

            try
            {
                URL url = new URL(stringUrl);
                HttpURLConnection c = (HttpURLConnection) url.openConnection();
                c.setRequestMethod("GET");
                c.setReadTimeout(15000);
                c.connect();
                BufferedReader reader = new BufferedReader(new InputStreamReader(c.getInputStream()));
                StringBuilder buf = new StringBuilder();
                String line;
                while ((line = reader.readLine()) != null)
                {
                    buf.append(line).append("\n");
                }
                jsonObject = new JSONObject(buf.toString());
            }
            catch (MalformedURLException e)
            {
                e.printStackTrace();
            }
            catch (ProtocolException e)
            {
                e.printStackTrace();
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }
            catch (JSONException e)
            {
                e.printStackTrace();
            }
        }

        return jsonObject;
    }

    public static JSONObject getAllTeams()
    {
        JSONObject jsonObject = null;
        //TODO: Надо подумать не будет ли тут замкнутого цикла
        while (jsonObject == null || jsonObject.length() < 1)
        {
            jsonObject = getAllTeamsFromServer();
        }
        return jsonObject;
    }

    public static JSONObject getFutureGames()
    {
        JSONObject jsonObject = null;
        //TODO: Надо подумать не будет ли тут замкнутого цикла
        while (jsonObject == null || jsonObject.length() < 1)
        {
            jsonObject = getFutureGamesFromServer();
        }
        return jsonObject;
    }

    public static JSONObject getLastResults()
    {
        JSONObject jsonObject = null;
        //TODO: Надо подумать не будет ли тут замкнутого цикла
        while (jsonObject == null || jsonObject.length() < 1)
        {
            jsonObject = getLastResultsFromServer();
        }
        return jsonObject;
    }

    private static JSONArray getJSONArrayByJSONObject(JSONObject jsonObject, String tag)
    {
        JSONArray jsonArray = null;
        try
        {
            jsonArray = (JSONArray) jsonObject.get(tag);
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }
        return jsonArray;
    }

    public static List<Team> parseTeam(JSONObject jsonObject)
    {
        List<Team> allTeams = new ArrayList<Team>();
        JSONArray jsonArray = getJSONArrayByJSONObject(jsonObject, "team");
        try
        {
            for (int i = 0; i < jsonArray.length(); i++)
            {
                JSONObject object = jsonArray.getJSONObject(i);
                Team team = new Team();
                team.setDraw(object.getString("draw"));
                team.setGamesPlayed(object.getString("gamesPlayed"));
                team.setGoalsLose(object.getString("goalsLose"));
                team.setGoalsScored(object.getString("goalsScored"));
                team.setLose(object.getString("lose"));
                team.setName(object.getString("name"));
                team.setPlace(object.getString("place"));
                team.setPoints(object.getString("points"));
                team.setWin(object.getString("win"));
                allTeams.add(team);
            }

        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }
        Collections.sort(allTeams);
        return allTeams;
    }

    public static List<Result> parseResults(JSONObject jsonObject)
    {
        List<Result> allResults = new ArrayList<Result>();
        return getResults(jsonObject, allResults);
    }

    private static List<Result> getResults(JSONObject jsonObject, List<Result> allResults)
    {
        JSONArray jsonArray = getJSONArrayByJSONObject(jsonObject, "result");
        try
        {
            for (int i = 0; i < jsonArray.length(); i++)
            {
                JSONObject object = jsonArray.getJSONObject(i);
                Result res = new Result();
                res.setHomeTeam(object.getString("homeTeam"));
                res.setGuestTeam(object.getString("guestTeam"));
                res.setDate(object.getString("date"));
                res.setTeamResult(object.getString("teamResult"));
                allResults.add(res);
            }
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }
        Collections.sort(allResults);
        return allResults;
    }

    public static List<Result> parseResults()
    {
        JSONObject jsonObject = getLastResults();

        List<Result> allResults = new ArrayList<Result>();
        return getResults(jsonObject, allResults);
    }


    public static int getResID(String name)
    {
        if (name == null)
        {
            return 0;
        }
        if (name.equals("Zenit"))
        {
            return R.drawable.zenit;
        }
        if (name.contains("CSKA Moscow"))
        {
            return R.drawable.cska_fk;
        }
        if (name.contains("FK Krasnodar"))
        {
            return R.drawable.krasnodar;
        }
        if (name.contains("Spartak Mos"))
        {
            return R.drawable.spartak_msk;
        }
        if (name.contains("Dynamo Moscow"))
        {
            return R.drawable.dinamo_moskva;
        }
        if (name.contains("Terek Groznyi"))
        {
            return R.drawable.terek;
        }
        if (name.contains("Kuban Krasnodar"))
        {
            return R.drawable.kuban_krasnodar;
        }
        if (name.contains("Lokomotiv Moscow"))
        {
            return R.drawable.loco_png;
        }
        if (name.contains("Rubin Kazan"))
        {
            return R.drawable.rubin_kazan;
        }
        if (name.contains("Mordovia Saransk"))
        {
            return R.drawable.mordovia_png;
        }
        if (name.contains("FK Ufa"))
        {
            return R.drawable.ufa_png;
        }
        if (name.contains("FK Ural"))
        {
            return R.drawable.ural_png;
        }
        if (name.contains("Amkar Perm"))
        {
            return R.drawable.amkar_png;
        }
        if (name.contains("FK Rostov"))
        {
            return R.drawable.rostov;
        }
        if (name.contains("Torpedo Moscow"))
        {
            return R.drawable.torpedo;
        }
        if (name.contains("Arsenal Tula"))
        {
            return R.drawable.arsenal_png;
        }
        return 0;
    }

    public static String getRussianTitle(String name)
    {
        if (name == null)
        {
            return "";
        }
        if (name.equals("Zenit"))
        {
            return "Зенит";
        }
        if (name.contains("CSKA Moscow"))
        {
            return "ЦСКА";
        }
        if (name.contains("FK Krasnodar"))
        {
            return "Краснодар";
        }
        if (name.contains("Spartak Mos"))
        {
            return "Спартак";
        }
        if (name.contains("Dynamo Moscow"))
        {
            return "Динамо";
        }
        if (name.contains("Terek Groznyi"))
        {
            return "Терек";
        }
        if (name.contains("Kuban Krasnodar"))
        {
            return "Кубань";
        }
        if (name.contains("Lokomotiv Moscow"))
        {
            return "Локомотив";
        }
        if (name.contains("Rubin Kazan"))
        {
            return "Рубин";
        }
        if (name.contains("Mordovia Saransk"))
        {
            return "Мордовия";
        }
        if (name.contains("FK Ufa"))
        {
            return "Уфа";
        }
        if (name.contains("FK Ural"))
        {
            return "Урал";
        }
        if (name.contains("Amkar Perm"))
        {
            return "Амкар";
        }
        if (name.contains("FK Rostov"))
        {
            return "Ростов";
        }
        if (name.contains("Torpedo Moscow"))
        {
            return "Торпедо";
        }
        if (name.contains("Arsenal Tula"))
        {
            return "Арсенал";
        }
        return "";
    }

    public static String getDate(String timeStamp)
    {
        Date date = new Date(Long.valueOf(timeStamp)*1000);
        SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yy");
        return sdf.format(date);
    }

    public static String getDateTime(String timeStamp)
    {
        Date date = new Date(Long.valueOf(timeStamp)*1000);
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
        return sdf.format(date);
    }

    public static JSONObject getJSONObjectByString(String lastLocalRequest)
    {
        JSONObject jsonObject = null;
        try
        {
            jsonObject = new JSONObject(lastLocalRequest);
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }
        return jsonObject;
    }
}

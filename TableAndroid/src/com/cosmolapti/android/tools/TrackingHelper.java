package com.cosmolapti.android.tools;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;


public class TrackingHelper
{

    private TrackingApplication application;

    public TrackingHelper(TrackingApplication application)
    {
        this.application = application;
    }

    public void sendScreen(String screenName)
    {
        Tracker t = application.getTracker(TrackingApplication.TrackerName.APP_TRACKER);
        // Set screen name.
        // Where path is a String representing the screen name.
        t.setScreenName(screenName);
                // Send a screen view.
        t.send(new HitBuilders.AppViewBuilder().build());
    }

    public void sendEvent(String category, String action, String label)
    {
        Tracker t = application.getTracker(TrackingApplication.TrackerName.APP_TRACKER);
        t.send(new HitBuilders.EventBuilder()
                .setCategory(category)
                .setAction(action)
                .setLabel(label)
                .build());
    }
}

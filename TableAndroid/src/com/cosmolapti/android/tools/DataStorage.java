package com.cosmolapti.android.tools;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import com.cosmolapti.android.bean.Result;
import com.cosmolapti.android.bean.Team;
import org.json.JSONObject;

import java.util.List;

public class DataStorage
{
    private static DataStorage instance;

    private static final String MAIN_TABLE = "lastAllTeams";
    private static final String FUTURE_GAMES = "futureGames";
    private static final String LAST_RESULTS = "lastResults";
    private static final String PREFERENCES_RFPL = "rfpl";


    private JSONObject mainTable;
    private JSONObject lastResults;
    private JSONObject futureGames;

    private List<Team> teams;
    private List<Result> results;
    private List<Result> scheduler;

    public DataStorage()
    {
    }

    public static DataStorage getInstance()
    {
        if (instance == null)
        {
            instance = new DataStorage();
        }
        return instance;
    }

    public JSONObject getMainTable()
    {
        return mainTable;
    }

    public void setMainTable(JSONObject mainTable)
    {
        this.mainTable = mainTable;
        setTeams(Service.parseTeam(mainTable));
    }

    public JSONObject getLastResults()
    {
        return lastResults;
    }

    public void setLastResults(JSONObject results)
    {
        this.lastResults = results;
        setResults(Service.parseResults(results));
    }

    public JSONObject getFutureGames()
    {
        return futureGames;
    }

    public void setFutureGames(JSONObject futureResults)
    {
        this.futureGames = futureResults;
        setScheduler(Service.parseResults(futureResults));
    }

    public boolean isAllDataUploaded()
    {
        return !((getMainTable()==null || getMainTable().length() == 0) ||
                (getLastResults() == null || getLastResults().length() == 0) ||
                        ( getFutureGames()==null || getFutureGames().length() == 0));
    }

    public List<Team> getTeams()
    {
        return teams;
    }

    private void setTeams(List<Team> teams)
    {
        this.teams = teams;
    }

    public List<Result> getResults()
    {
        return results;
    }

    private void setResults(List<Result> results)
    {
        this.results = results;
    }

    public List<Result> getScheduler()
    {
        return scheduler;
    }

    private void setScheduler(List<Result> scheduler)
    {
        this.scheduler = scheduler;
    }

    public Team getTeamInfo(String teamName)
    {
        for (Team team : getTeams())
        {
            if (teamName.equals(team.getName()))
            {
                return team;
            }
        }
        return null;
    }

    public void loadData(Context context)
    {
        if (isConnectionAvailable(context))
        {
            this.setMainTable(Service.getAllTeams());
            this.setLastResults(Service.getLastResults());
            this.setFutureGames(Service.getFutureGames());
            saveLastServerRequest(context);
        }
        else
        {
            SharedPreferences sPref = context.getSharedPreferences(PREFERENCES_RFPL, Context.MODE_PRIVATE);
            if (sPref!=null)
            {
                String allTeams = sPref.getString(MAIN_TABLE, "");
                if (allTeams!=null && !allTeams.isEmpty())
                this.setMainTable(Service.getJSONObjectByString(allTeams));

                String lastResults = sPref.getString(LAST_RESULTS, "");
                if (lastResults!=null && !lastResults.isEmpty())
                this.setLastResults(Service.getJSONObjectByString(lastResults));

                String futureGames = sPref.getString(FUTURE_GAMES, "");
                if (futureGames!=null && !futureGames.isEmpty())
                this.setFutureGames(Service.getJSONObjectByString(futureGames));
            }
        }
    }

    void saveLastServerRequest(Context context)
    {
        SharedPreferences sPref = context.getSharedPreferences(PREFERENCES_RFPL, Context.MODE_PRIVATE);
        SharedPreferences.Editor ed = sPref.edit();
        ed.putString(MAIN_TABLE, this.getMainTable().toString());
        ed.putString(LAST_RESULTS, this.getLastResults().toString());
        ed.putString(FUTURE_GAMES, this.getFutureGames().toString());
        ed.apply();
    }
    private boolean isConnectionAvailable(Context context)
    {

        ConnectivityManager conMgr = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = conMgr.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected() && activeNetworkInfo.isAvailable();
    }

}

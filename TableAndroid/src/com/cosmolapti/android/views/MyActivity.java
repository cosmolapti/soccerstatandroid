package com.cosmolapti.android.views;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.util.TypedValue;
import android.view.*;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.cosmolapti.android.R;
import com.cosmolapti.android.tools.DataStorage;
import com.cosmolapti.android.tools.Service;
import com.cosmolapti.android.tools.TrackingApplication;
import com.cosmolapti.android.tools.TrackingHelper;

import java.util.Date;

public class MyActivity extends ActionBarActivity
{
    private ViewPager viewPager;
    private ActionBar actionBar;

    private TrackingHelper trackingHelper;

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        trackingHelper = new TrackingHelper((TrackingApplication) getApplication());
        trackingHelper.sendScreen("Main_Activity");

        actionBar = getSupportActionBar();

        actionBar.setDisplayShowHomeEnabled(true);
        actionBar.setLogo(R.drawable.logo);
        actionBar.setDisplayUseLogoEnabled(true);

        createTabs();
    }



    private void createTabs()
    {
        FrameLayout ff = (FrameLayout) this.findViewById(R.id.frameLayout);
        ff.removeView(this.findViewById(R.id.linearLayoutError));
        DataStorage dataStorage = DataStorage.getInstance();
        dataStorage.loadData(this);
        if (dataStorage.isAllDataUploaded())
        {
            // Specify that tabs should be displayed in the action bar.
            actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);

            TabPagerAdapter tpa = new TabPagerAdapter(getSupportFragmentManager());

            viewPager = (ViewPager) findViewById(R.id.pager);
            viewPager.setAdapter(tpa);
            viewPager.setOnPageChangeListener(
                    new ViewPager.SimpleOnPageChangeListener()
                    {
                        @Override
                        public void onPageSelected(int position)
                        {
                            // When swiping between pages, select the
                            // corresponding tab.
                            actionBar.setSelectedNavigationItem(position);
                        }
                    });

            // Create a tab listener that is called when the user changes tabs.
            ActionBar.TabListener tabListener = new ActionBar.TabListener()
            {
                public void onTabSelected(ActionBar.Tab tab, FragmentTransaction ft)
                {
                    viewPager.setCurrentItem(tab.getPosition());
                    trackingHelper.sendScreen(tab.getText().toString());
                }

                public void onTabUnselected(ActionBar.Tab tab, FragmentTransaction ft)
                {
                    // hide the given tab
                }

                public void onTabReselected(ActionBar.Tab tab, FragmentTransaction ft)
                {
                    // probably ignore this event
                }
            };

            // Add 3 tabs, specifying the tab's text and TabListener
            ActionBar.Tab tableTab = actionBar.newTab();
            ActionBar.Tab resultTab = actionBar.newTab();
            ActionBar.Tab futureTab = actionBar.newTab();

            actionBar.addTab(tableTab.setText("Таблица").setTabListener(tabListener));
            actionBar.addTab(resultTab.setText(" Результаты ").setTabListener(tabListener));
            actionBar.addTab(futureTab.setText(" Календарь ").setTabListener(tabListener));
        }
        else
        {
            addFirstOfflineModeError();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        // Inflate the menu items for use in the action bar
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    private void addFirstOfflineModeError()
    {
        FrameLayout frameLayout = (FrameLayout) this.findViewById(R.id.frameLayout);
        Float dimension = getResources().getDimension(R.dimen.textsize);

        LinearLayout ll = new LinearLayout(this);
        ll.setOrientation(LinearLayout.VERTICAL);
        ll.setId(R.id.linearLayoutError);

        final TextView errorTextView = new TextView(this);
        errorTextView.setText("Подключение отсутствует");
        errorTextView.setGravity(Gravity.CENTER);
        errorTextView.setTextSize(TypedValue.COMPLEX_UNIT_PX, (dimension + 10));
        errorTextView.setTextColor(Color.WHITE);

        final Button button = new Button(this);
        button.setText(" Обновить ");
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        button.setLayoutParams(params);

        button.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                createTabs();
            }
        });

        ll.setGravity(Gravity.CENTER);
        ll.addView(errorTextView);
        ll.addView(button);
        frameLayout.addView(ll);
    }



    //runs without a timer by reposting this handler at the end of the runnable
    Handler timerHandler = new Handler();
    Runnable timerRunnable = new Runnable()
    {
        @Override
        public void run()
        {
            loadData();
            timerHandler.postDelayed(this, 1000 * 60 * 15);
        }
    };

    @Override
    public void onResume()
    {
        super.onResume();
        timerHandler.postDelayed(timerRunnable, 0);
    }

    @Override
    public void onPause()
    {
        super.onPause();
        timerHandler.removeCallbacks(timerRunnable);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        trackingHelper.sendEvent("user_action", "OnClick", "Refresh btn");
        // Handle presses on the action bar items
        switch (item.getItemId())
        {
            case R.id.action_refresh:
                loadData();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void loadData()
    {
        DataStorage.getInstance().loadData(this);
    }

    public TrackingHelper getTrackingHelper()
    {
        return trackingHelper;
    }
}

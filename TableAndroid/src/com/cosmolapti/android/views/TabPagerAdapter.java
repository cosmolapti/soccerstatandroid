package com.cosmolapti.android.views;


import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

public class TabPagerAdapter extends FragmentStatePagerAdapter
{
    private TableFragment tf;
    private ResultFragment rf;
    private FutureFragment ff;


    public TabPagerAdapter(FragmentManager fm)
    {
        super(fm);
    }

    public Fragment getItem(int i) {
        switch (i)
        {
            case 0:
                return getTableFragment();
            case 1:
                return getResultFragment();
            case 2:
                return getFutureFragment();
        }
        return null;
    }

    TableFragment getTableFragment()
    {
        if (tf==null)
            tf = new TableFragment();
        return tf;
    }

    ResultFragment getResultFragment()
    {
        if (rf==null)
            rf = new ResultFragment();
        return rf;
    }

    FutureFragment getFutureFragment()
    {
        if (ff==null)
            ff = new FutureFragment();
        return ff;
    }

    @Override
    public int getCount() {
        return 3;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position)
        {
            case 0:
                return "Main Tab";
            case 1:
                return "Result Tab";
            case 2:
                return "Future Tab";
        }
        return "";
    }
}


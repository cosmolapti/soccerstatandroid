package com.cosmolapti.android.views;

import android.content.res.Resources;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import com.cosmolapti.android.R;
import com.cosmolapti.android.bean.Result;
import com.cosmolapti.android.tools.DataStorage;
import com.cosmolapti.android.tools.Service;

import java.util.List;

public class ResultFragment extends Fragment
{
    TableLayout tableLayout;
    TableRow.LayoutParams tableRowParams;

    float dimension;
    View resultTab;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        setHasOptionsMenu(true);
        resultTab = inflater.inflate(R.layout.result_fragment, container, false);
        super.onCreate(savedInstanceState);
        dimension = getResources().getDimension(R.dimen.textsize);

        tableLayout = (TableLayout) resultTab.findViewById(R.id.resultTableLayout);
        tableRowParams = new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.MATCH_PARENT, 1.0f);
        createResultTable();

        return resultTab;
    }

    public void createResultTable()
    {
        FrameLayout ff = (FrameLayout) resultTab.findViewById(R.id.resultFrameLayout);
        ff.removeView(resultTab.findViewById(R.id.linearLayoutError));
        tableLayout.removeAllViews();

        List<Result> lastResults = DataStorage.getInstance().getResults();
        TableLayout resultTableLayout = (TableLayout) resultTab.findViewById(R.id.resultTableLayout);


        Resources res = getResources();
        int row1 = res.getColor(R.color.row1);
        int row2 = res.getColor(R.color.row2);
        boolean zebra = false;

        String prevDate = null;

        for (Result lastResult : lastResults)
        {

            if (!(Service.getDate(lastResult.getDate())).equals(prevDate))
            {
                TableRow tr = new TableRow(resultTab.getContext());
                tr.setLayoutParams(tableRowParams);
                resultTableLayout.addView(tr);

                TextView editText = new TextView(resultTab.getContext());
                TableRow.LayoutParams params = new TableRow.LayoutParams();
                params.span = 4;

                String text = Service.getDate(lastResult.getDate());
//                SpannableString content = new SpannableString(text);
//                content.setSpan(new UnderlineSpan(), 0, text.length(), 0);
                editText.setText(text);
                editText.setTextColor(getResources().getColor(R.color.white));

                editText.setLayoutParams(params);
                editText.setTextSize(TypedValue.COMPLEX_UNIT_PX, (dimension));
                editText.setTypeface(Typeface.DEFAULT_BOLD);
                editText.setPadding(0, 10, 0, 0);

                tr.addView(editText);
            }

            TableRow tr = new TableRow(resultTab.getContext());
            tr.setLayoutParams(tableRowParams);
            int color = row1;

            if (zebra)
            {
                zebra = false;
            }
            else
            {
                color = row2;
                zebra = true;
            }
            tr.setBackgroundColor(color);
            resultTableLayout.addView(tr);

            prevDate = Service.getDate(lastResult.getDate());
            //Date
            TextView date = new TextView(resultTab.getContext());
            date.setText(Service.getDateTime(lastResult.getDate()));
            date.setLayoutParams(tableRowParams);
            date.setTextSize(TypedValue.COMPLEX_UNIT_PX, (dimension));
            date.setPadding(2, 0, 2, 0);
            tr.addView(date);

            //LOGO
            ImageView homeLogo = new ImageView(resultTab.getContext());
            int logoID = Service.getResID(lastResult.getHomeTeam());
            homeLogo.setImageResource(logoID);
            homeLogo.setLayoutParams(new TableRow.LayoutParams(getResources().getInteger(R.integer.logoSize), getResources().getInteger(R.integer.logoSize)));
            tr.addView(homeLogo);

            TextView homeTeam = new TextView(resultTab.getContext());
            homeTeam.setText(Service.getRussianTitle(lastResult.getHomeTeam()));
            homeTeam.setLayoutParams(tableRowParams);
            homeTeam.setPadding(2, 0, 5, 0);
            homeTeam.setTextSize(TypedValue.COMPLEX_UNIT_PX, (dimension));
            tr.addView(homeTeam);

            //RESULT
            TextView result = new TextView(resultTab.getContext());
            result.setText(lastResult.getTeamResult());
            result.setLayoutParams(tableRowParams);
            result.setTextSize(TypedValue.COMPLEX_UNIT_PX, (dimension));
            result.setTypeface(null, Typeface.BOLD);

            tr.addView(result);

            //LOGO
            ImageView guestLogo = new ImageView(resultTab.getContext());
            int guestLogoId = Service.getResID(lastResult.getGuestTeam());
            guestLogo.setImageResource(guestLogoId);
            guestLogo.setLayoutParams(new TableRow.LayoutParams(getResources().getInteger(R.integer.logoSize), getResources().getInteger(R.integer.logoSize)));
            guestLogo.setPadding(5, 0, 0, 0);
            tr.addView(guestLogo);

            TextView guestTeam = new TextView(resultTab.getContext());
            guestTeam.setText(Service.getRussianTitle(lastResult.getGuestTeam()));
            guestTeam.setLayoutParams(tableRowParams);
            guestTeam.setTextSize(TypedValue.COMPLEX_UNIT_PX, (dimension));
            guestTeam.setPadding(2, 0, 2, 0);
            tr.addView(guestTeam);
        }
    }
}

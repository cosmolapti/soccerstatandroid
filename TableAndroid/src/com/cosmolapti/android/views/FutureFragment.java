package com.cosmolapti.android.views;

import android.content.res.Resources;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import com.cosmolapti.android.R;
import com.cosmolapti.android.bean.Result;
import com.cosmolapti.android.tools.DataStorage;
import com.cosmolapti.android.tools.Service;

import java.util.Collections;
import java.util.List;

public class FutureFragment extends Fragment
{
    TableLayout tableLayout;
    TableRow.LayoutParams tableRowParams;

    float dimension;
    View futureTab;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        setHasOptionsMenu(true);
        futureTab = inflater.inflate(R.layout.future_fragment, container, false);
        super.onCreate(savedInstanceState);
        dimension = getResources().getDimension(R.dimen.textsize);

        tableLayout = (TableLayout) futureTab.findViewById(R.id.futureTableLayout);
        tableRowParams = new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.MATCH_PARENT, 1.0f);
        createResultTable();

        return futureTab;
    }

    public void createResultTable()
    {
        FrameLayout ff = (FrameLayout) futureTab.findViewById(R.id.futureFrameLayout);
        ff.removeView(futureTab.findViewById(R.id.linearLayoutError));
        tableLayout.removeAllViews();


        List<Result> lastResults = DataStorage.getInstance().getScheduler();
        TableLayout resultTableLayout = (TableLayout) futureTab.findViewById(R.id.futureTableLayout);


        Resources res = getResources();
        int row1 = res.getColor(R.color.row1);
        int row2 = res.getColor(R.color.row2);
        boolean zebra = false;

        String prevDate = null;

        Collections.reverse(lastResults);
        for (Result lastResult : lastResults)
        {

            if (!Service.getDate(lastResult.getDate()).equals(prevDate))
            {
                TableRow tr = new TableRow(futureTab.getContext());
                tr.setLayoutParams(tableRowParams);
                resultTableLayout.addView(tr);

                TextView editText = new TextView(futureTab.getContext());
                TableRow.LayoutParams params = new TableRow.LayoutParams();
                params.span = 4;
                editText.setText(Service.getDate(lastResult.getDate()));
                editText.setLayoutParams(params);
                editText.setTextSize(TypedValue.COMPLEX_UNIT_PX, (dimension));
                editText.setTypeface(Typeface.DEFAULT_BOLD);
                editText.setTextColor(getResources().getColor(R.color.white));
                editText.setPadding(0, 10, 0, 0);
                tr.addView(editText);
            }

            TableRow tr = new TableRow(futureTab.getContext());
            tr.setLayoutParams(tableRowParams);
            int color = row1;

            if (zebra)
            {
                zebra = false;
            }
            else
            {
                color = row2;
                zebra = true;
            }
            tr.setBackgroundColor(color);
            resultTableLayout.addView(tr);

            prevDate = Service.getDate(lastResult.getDate());

            //Date
            TextView date = new TextView(futureTab.getContext());
//                date.setText(Service.getDateTime(lastResult.getDate()));
            date.setLayoutParams(tableRowParams);
            date.setTextSize(TypedValue.COMPLEX_UNIT_PX, (dimension));
            date.setPadding(2, 0, 2, 0);
            tr.addView(date);

            //LOGO
            ImageView homeLogo = new ImageView(futureTab.getContext());
            int logoID = Service.getResID(lastResult.getHomeTeam());
            homeLogo.setImageResource(logoID);
            homeLogo.setLayoutParams(new TableRow.LayoutParams(getResources().getInteger(R.integer.logoSize), getResources().getInteger(R.integer.logoSize)));
            tr.addView(homeLogo);

            TextView homeTeam = new TextView(futureTab.getContext());
            homeTeam.setText(Service.getRussianTitle(lastResult.getHomeTeam()));
            homeTeam.setLayoutParams(tableRowParams);
            homeTeam.setPadding(5, 0, 5, 0);
            homeTeam.setTextSize(TypedValue.COMPLEX_UNIT_PX, (dimension));
            tr.addView(homeTeam);

            //RESULT
            TextView result = new TextView(futureTab.getContext());
            result.setText(lastResult.getTeamResult());
            result.setLayoutParams(tableRowParams);
            result.setTextSize(TypedValue.COMPLEX_UNIT_PX, (dimension));
            result.setTypeface(null, Typeface.BOLD);

            tr.addView(result);

            //LOGO
            ImageView guestLogo = new ImageView(futureTab.getContext());
            int guestLogoId = Service.getResID(lastResult.getGuestTeam());
            guestLogo.setImageResource(guestLogoId);
            guestLogo.setLayoutParams(new TableRow.LayoutParams(getResources().getInteger(R.integer.logoSize), getResources().getInteger(R.integer.logoSize)));
            tr.addView(guestLogo);

            TextView guestTeam = new TextView(futureTab.getContext());
            guestTeam.setText(Service.getRussianTitle(lastResult.getGuestTeam()));
            guestTeam.setLayoutParams(tableRowParams);
            guestTeam.setTextSize(TypedValue.COMPLEX_UNIT_PX, (dimension));
            guestTeam.setPadding(5, 0, 0, 0);
            tr.addView(guestTeam);
        }
    }
}

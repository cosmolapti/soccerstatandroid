package com.cosmolapti.android.views;

import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import com.cosmolapti.android.R;
import com.cosmolapti.android.bean.Team;
import com.cosmolapti.android.tools.DataStorage;
import com.cosmolapti.android.tools.Service;

import java.util.List;

public class TableFragment extends Fragment
{
    TableLayout tableLayout;
    TableRow.LayoutParams tableRowParams;
    float dimension;
    View tableTab;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        tableTab = inflater.inflate(R.layout.table_fragment, container, false);
        super.onCreate(savedInstanceState);
        dimension = getResources().getDimension(R.dimen.textsize);

        tableLayout = (TableLayout) tableTab.findViewById(R.id.tableLayout);
        tableRowParams = new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.MATCH_PARENT, 1.0f);
        createMainTable();
        return tableTab;
    }

    public void createMainTable()
    {
        FrameLayout ff = (FrameLayout) tableTab.findViewById(R.id.frameLayout);
        ff.removeView(tableTab.findViewById(R.id.linearLayoutError));

        tableLayout.removeAllViews();

        TableRow header = new TableRow(tableTab.getContext());
        header.setLayoutParams(tableRowParams);
        setHeader(header);

        List<Team> allTeam = DataStorage.getInstance().getTeams();
        fillMainTable(allTeam);
    }

    private void fillMainTable(List<Team> allTeam)
    {
        Resources res = getResources();
        int row1 = res.getColor(R.color.row1);
        int row2 = res.getColor(R.color.row2);
        boolean zebra = false;
        for (Team team : allTeam)
        {
            TableRow tr = new TableRow(tableTab.getContext());

            tr.setLayoutParams(tableRowParams);
            int color = row1;

            if (zebra)
            {
                zebra = false;
            }
            else
            {
                color = row2;
                zebra = true;
            }
            tr.setBackgroundColor(color);
            tableLayout.addView(tr);

            //Place
            TextView placeTextView = new TextView(tableTab.getContext());
            placeTextView.setText(team.getPlace());
            placeTextView.setLayoutParams(tableRowParams);

            placeTextView.setTextSize(TypedValue.COMPLEX_UNIT_PX, dimension);

            placeTextView.setPadding(10, 0, 0, 0);
            tr.addView(placeTextView);

            //LOGO
            ImageView imageView = new ImageView(tableTab.getContext());
            int logoID = Service.getResID(team.getName());
            imageView.setImageResource(logoID);
            imageView.setLayoutParams(new TableRow.LayoutParams(getResources().getInteger(R.integer.logoSize), getResources().getInteger(R.integer.logoSize)));
            tr.addView(imageView);

            //Name
            TextView nameTextView = new TextView(tableTab.getContext());
            nameTextView.setText(Service.getRussianTitle(team.getName()));
            TableRow.LayoutParams nameTextViewLP = new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.MATCH_PARENT, 1.2f);
            nameTextView.setLayoutParams(nameTextViewLP);
            nameTextView.setPadding(10, 0, 0, 0);
            nameTextView.setTextSize(TypedValue.COMPLEX_UNIT_PX, dimension);
            nameTextView.setTextColor(getResources().getColor(R.color.white));
            nameTextView.setTypeface(Typeface.DEFAULT_BOLD);
            tr.addView(nameTextView);

            //Points
            TextView ptTextView = new TextView(tableTab.getContext());
            ptTextView.setText(team.getPoints());
            tr.addView(ptTextView);
            ptTextView.setLayoutParams(tableRowParams);
            ptTextView.setTextSize(TypedValue.COMPLEX_UNIT_PX, dimension);
            ptTextView.setTypeface(null, Typeface.BOLD);

            //Game Played
            TextView gpTextView = new TextView(tableTab.getContext());
            gpTextView.setText(team.getGamesPlayed());
            tr.addView(gpTextView);
            gpTextView.setTextSize(TypedValue.COMPLEX_UNIT_PX, dimension);
            gpTextView.setLayoutParams(tableRowParams);

            //Win
            TextView winTextView = new TextView(tableTab.getContext());
            winTextView.setText(team.getWin());
            tr.addView(winTextView);
            winTextView.setTextSize(TypedValue.COMPLEX_UNIT_PX, dimension);
            winTextView.setLayoutParams(tableRowParams);

            //Draw
            TextView drawTextView = new TextView(tableTab.getContext());
            drawTextView.setText(team.getDraw());
            tr.addView(drawTextView);
            drawTextView.setTextSize(TypedValue.COMPLEX_UNIT_PX, dimension);
            drawTextView.setLayoutParams(tableRowParams);

            //Lose
            TextView loseTextView = new TextView(tableTab.getContext());
            loseTextView.setText(team.getLose());
            tr.addView(loseTextView);
            loseTextView.setLayoutParams(tableRowParams);
            loseTextView.setTextSize(TypedValue.COMPLEX_UNIT_PX, dimension);

            //Goal scored
            TextView goalScoredTextView = new TextView(tableTab.getContext());
            goalScoredTextView.setText(team.getGoalsScored());
            tr.addView(goalScoredTextView);
            goalScoredTextView.setLayoutParams(tableRowParams);
            goalScoredTextView.setTextSize(TypedValue.COMPLEX_UNIT_PX, dimension);

            //Goal Lose
            TextView goalLoseTextView = new TextView(tableTab.getContext());
            goalLoseTextView.setText(team.getGoalsLose());
            tr.addView(goalLoseTextView);
            goalLoseTextView.setLayoutParams(tableRowParams);
            goalLoseTextView.setTextSize(TypedValue.COMPLEX_UNIT_PX, dimension);
        }
    }

    private static String getSizeName(Context context)
    {
        int screenLayout = context.getResources().getConfiguration().screenLayout;
        screenLayout &= Configuration.SCREENLAYOUT_SIZE_MASK;

        switch (screenLayout)
        {
            case Configuration.SCREENLAYOUT_SIZE_SMALL:
                return "small";
            case Configuration.SCREENLAYOUT_SIZE_NORMAL:
                return "normal";
            case Configuration.SCREENLAYOUT_SIZE_LARGE:
                return "large";
            case 4: // Configuration.SCREENLAYOUT_SIZE_XLARGE is API >= 9
                return "xlarge";
            default:
                return "undefined";
        }
    }

    private void setHeader(TableRow th)
    {
        //Place
        TextView placeTextView = new TextView(tableTab.getContext());
        placeTextView.setText("");
        placeTextView.setLayoutParams(tableRowParams);
        placeTextView.setPadding(10, 0, 0, 0);
        th.addView(placeTextView);

        //Logo
        TextView logoTextView = new TextView(tableTab.getContext());
        logoTextView.setText("");
        logoTextView.setLayoutParams(new TableRow.LayoutParams(getResources().getInteger(R.integer.logoSize), getResources().getInteger(R.integer.logoSize)));
        th.addView(logoTextView);

        //Name
        TextView nameTextView = new TextView(tableTab.getContext());

//        nameTextView.setText(getSizeName(tableTab.getContext()));

        TableRow.LayoutParams nameTextViewLP = new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.MATCH_PARENT, 1.2f);
        nameTextView.setLayoutParams(nameTextViewLP);
        nameTextView.setPadding(10, 0, 0, 0);
        th.addView(nameTextView);

        //Game Points
        TextView ptTextView = new TextView(tableTab.getContext());
        ptTextView.setText("О");
        th.addView(ptTextView);
        ptTextView.setLayoutParams(tableRowParams);
        ptTextView.setTextSize(TypedValue.COMPLEX_UNIT_PX, dimension);
        ptTextView.setTypeface(null, Typeface.BOLD);

        //Game Played
        TextView gpTextView = new TextView(tableTab.getContext());
        gpTextView.setText("И");
        th.addView(gpTextView);
        gpTextView.setLayoutParams(tableRowParams);
        gpTextView.setTextSize(TypedValue.COMPLEX_UNIT_PX, dimension);
        gpTextView.setTypeface(null, Typeface.BOLD);

        //Win
        TextView winTextView = new TextView(tableTab.getContext());
        winTextView.setText("В");
        th.addView(winTextView);
        winTextView.setLayoutParams(tableRowParams);
        winTextView.setTextSize(TypedValue.COMPLEX_UNIT_PX, dimension);
        winTextView.setTypeface(null, Typeface.BOLD);

        //Draw
        TextView drawTextView = new TextView(tableTab.getContext());
        drawTextView.setText("Н");
        th.addView(drawTextView);
        drawTextView.setLayoutParams(tableRowParams);
        drawTextView.setTextSize(TypedValue.COMPLEX_UNIT_PX, dimension);
        drawTextView.setTypeface(null, Typeface.BOLD);

        //Lose
        TextView loseTextView = new TextView(tableTab.getContext());
        loseTextView.setText("П");
        th.addView(loseTextView);
        loseTextView.setLayoutParams(tableRowParams);
        loseTextView.setTextSize(TypedValue.COMPLEX_UNIT_PX, dimension);
        loseTextView.setTypeface(null, Typeface.BOLD);

        //Goal scored
        TextView goalScoredTextView = new TextView(tableTab.getContext());
        goalScoredTextView.setText("ГЗ");
        th.addView(goalScoredTextView);
        goalScoredTextView.setLayoutParams(tableRowParams);
        goalScoredTextView.setTextSize(TypedValue.COMPLEX_UNIT_PX, dimension);
        goalScoredTextView.setTypeface(null, Typeface.BOLD);

        //Goal Lose
        TextView goalLoseTextView = new TextView(tableTab.getContext());
        goalLoseTextView.setText("ГП");
        th.addView(goalLoseTextView);
        goalLoseTextView.setLayoutParams(tableRowParams);
        goalLoseTextView.setTextSize(TypedValue.COMPLEX_UNIT_PX, dimension);
        goalLoseTextView.setTypeface(null, Typeface.BOLD);
    }
}
